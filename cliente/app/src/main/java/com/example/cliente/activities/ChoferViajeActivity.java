package com.example.cliente.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cliente.MyApplication;
import com.example.cliente.R;
import com.example.cliente.dataServices.ViajeService;
import com.example.cliente.databinding.ActivityChoferViajeBinding;
import com.example.cliente.fragments.ChoferConfirmCancelarViajeDialogFragment;
import com.example.cliente.fragments.ChoferSelectSiguienteParadaDialogFragment;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Parada;
import com.example.cliente.models.Viaje;
import com.example.cliente.services.GeofencesService;
import com.example.cliente.viewModels.ChoferViajeActivityViewModel;

public class ChoferViajeActivity extends AppCompatActivity implements ChoferSelectSiguienteParadaDialogFragment.ChoferSelectSiguienteParadaDialogListener, ChoferConfirmCancelarViajeDialogFragment.ChoferConfirmCancelarViajeDialogListener {

    String linea_id;
    Viaje viaje;
    Parada siguiente_parada;

    private ChoferViajeActivityViewModel choferViajeActivityViewModel;
    private ActivityChoferViajeBinding binding;

    enum EstadoMonitoreo {
        POR_EMPEZAR ("Por empezar"),
        MONITOREANDO ("Monitoreando"),
        PAUSADO ("Pausado"),
        POR_REANUDAR ("Por reanudar"),
        CANCELADO ("Cancelado");


        private String nombre;

        EstadoMonitoreo(String nombre){
            this.nombre = nombre;
        }

        @Override
        public String toString() {
            return this.nombre;
        }
    }

    EstadoMonitoreo estadoMonitoreo;
    TextView estado;
    ImageButton actionButton, cancelarViajeButton;

    private EstadoMonitoreo cancelarViaje() {
        this.choferViajeActivityViewModel.cancelarViaje(this.linea_id, this.viaje.id)
                    .observe(ChoferViajeActivity.this, (Viaje viaje) -> {
                        this.viaje = viaje;

                        // Una vez que se cancela el viaje
                        // la actividad se finaliza
                        this.finish();
                    });
        return EstadoMonitoreo.CANCELADO;
    }

    private EstadoMonitoreo monitorearViaje() {
        actionButton.setImageResource(android.R.drawable.ic_media_pause);
        cancelarViajeButton.setVisibility(View.VISIBLE);
        this.choferViajeActivityViewModel.createViaje(linea_id)
                .observe( ChoferViajeActivity.this, (Viaje viaje) -> {
                    Log.i("VIAJEID", viaje.id);
                    this.viaje = viaje;

                    GeofencesService.getInstance().addGeofence(this.viaje.siguienteParada);

                    this.updateViajeInfo();
                });
        return EstadoMonitoreo.MONITOREANDO;
    }

    private EstadoMonitoreo pausarViaje() {
        actionButton.setImageResource(android.R.drawable.ic_menu_slideshow);
        this.choferViajeActivityViewModel.pausarViaje(this.linea_id, this.viaje.id)
                .observe(ChoferViajeActivity.this,(Viaje viaje) -> {
                    this.viaje = viaje;
                });
        return EstadoMonitoreo.PAUSADO;
    }

    private EstadoMonitoreo elegirProximaParadaViaje() {
        actionButton.setImageResource(android.R.drawable.ic_menu_info_details);
        return EstadoMonitoreo.POR_REANUDAR;
    }

    private EstadoMonitoreo reanudarViaje() {
        actionButton.setImageResource(android.R.drawable.ic_media_pause);

        ChoferSelectSiguienteParadaDialogFragment dialog = new ChoferSelectSiguienteParadaDialogFragment();
        this.binding.pb.setVisibility(View.VISIBLE);
        this.choferViajeActivityViewModel.getLinea(this.linea_id).observe(this, (Linea linea) -> {
            this.binding.pb.setVisibility(View.GONE);
            dialog.setParadas(linea.getParadas());
            dialog.show(getSupportFragmentManager(), "SELECT_LINEA_DIALOG");
        });
        return EstadoMonitoreo.MONITOREANDO;
    }


    public void onDialogSiguienteParadaClick(DialogFragment dialog) {
        ChoferSelectSiguienteParadaDialogFragment ChoferSelectSiguienteParadaDialogFragment = (ChoferSelectSiguienteParadaDialogFragment) dialog;

        siguiente_parada = ChoferSelectSiguienteParadaDialogFragment.getParada();

        ViajeService.continuarViaje(this.linea_id, this.viaje.id, siguiente_parada.getId())
                .observe(ChoferViajeActivity.this, (Viaje viaje) -> {
                    Log.i("VIAJE", viaje.lineaId);
                    this.viaje = viaje;

                    this.updateViajeInfo();
                });
    }

    public void onDialogCancelarClick(DialogFragment dialog) {
        //parada = null;
        Toast.makeText(this, "CHOFER ACTIVITY - CANCELADO", Toast.LENGTH_SHORT).show();
    }





    public void onClickCancelarViajeButton(View view) {
        ChoferConfirmCancelarViajeDialogFragment dialog = new ChoferConfirmCancelarViajeDialogFragment();
        dialog.show(getSupportFragmentManager(), "CONFIRM_CANCEL_VIAJE");
    }


    public void onClickMonitoreoButton(View view) {

        // En los metodos de [monitorear|pausar|elegirProximaParada|reanudar]Viaje
        // es donde se debería hacer uso del Viaje service

        switch (estadoMonitoreo) {
            case POR_EMPEZAR:
                estadoMonitoreo = monitorearViaje();
                break;

            case MONITOREANDO:
                estadoMonitoreo = pausarViaje();
                break;

            case PAUSADO:
                estadoMonitoreo = elegirProximaParadaViaje();
                break;

            case POR_REANUDAR:
                estadoMonitoreo = reanudarViaje();
                break;
        }

    }

    private void setViewModel() {
        this.choferViajeActivityViewModel = ViewModelProviders
                .of(this)
                .get(ChoferViajeActivityViewModel.class);

        this.binding = DataBindingUtil
                .setContentView(this, R.layout.activity_chofer_viaje);

        this.binding.setLifecycleOwner(this);
        this.binding.setViewmodel(this.choferViajeActivityViewModel);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chofer_viaje);

        // Setear el toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);

        setViewModel();

        // Mostrar progress bar (or spinner)
        this.binding.pb.setVisibility(View.VISIBLE);

        linea_id = getIntent().getStringExtra("EXTRA_LINEA");

        this.choferViajeActivityViewModel.getLinea(linea_id).observe(this, (Linea linea) -> {

            // Ocultar spinner
            this.binding.pb.setVisibility(View.GONE);

            toolbar.setTitle(linea.getNombre());
            setSupportActionBar(toolbar);

        });

        estadoMonitoreo = EstadoMonitoreo.POR_EMPEZAR;

        actionButton = (ImageButton) findViewById(R.id.actionButton);

        actionButton.setImageResource(android.R.drawable.ic_media_play);

        cancelarViajeButton = (ImageButton) findViewById(R.id.cancelarViajeButton);

    }

    private void updateViajeInfo(){
        TextView anterior, siguiente, proxima;

        anterior = (TextView) findViewById(R.id.anteriorParadaText);
        anterior.setText(this.viaje.siguienteParada.getAnteriorId());

        siguiente = (TextView) findViewById(R.id.siguienteParadaText);
        siguiente.setText(this.viaje.siguienteParada.getId());

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        estadoMonitoreo = cancelarViaje();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        dialog.dismiss();
    }


}
