package com.example.cliente.activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.cliente.R;
import com.example.cliente.dataServices.LineaService;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Parada;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    String lineaId;
    List<Parada> paradas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Bundle extras = this.getIntent().getExtras();
        lineaId = (String) extras.get("lineaId");

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng medio = new LatLng(-43.259099, -65.297703);


        LineaService.getLineaById(this.lineaId)
                .observe(this, (Linea linea) -> {

                    this.paradas = linea.getParadas();
                    for (Parada parada: this.paradas) {
                        Log.i("PARADAS", parada.toString());
                        Marker m = mMap.addMarker(new MarkerOptions()
                                .position(parada.getPosicion())
                                .title("LA PARADA")
                                .snippet(parada.toString()));
                        m.showInfoWindow();
                    }
                });

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(medio,14));
        mMap.setBuildingsEnabled(true);

    }
}
