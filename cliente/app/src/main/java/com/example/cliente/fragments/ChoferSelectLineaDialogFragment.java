package com.example.cliente.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.example.cliente.R;
import com.example.cliente.models.Linea;

import java.util.ArrayList;
import java.util.List;

public class ChoferSelectLineaDialogFragment extends DialogFragment {

    private Linea linea;
    private List<Linea> lineas;
    ChoferSelectLineaDialogListener listener;

    public interface ChoferSelectLineaDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    public Linea getLinea() {
        return linea;
    }

    public void setLinea(Linea linea) {
        this.linea = linea;
    }

    public List<Linea> getLineas() {
        return lineas;
    }

    public void setLineas(List<Linea> lineas) {
        this.lineas = lineas;
    }

    private String[] getLineaStringArray(List<Linea> lineas) {
        ArrayList<String> rArr = new ArrayList<String>();

        for (Linea linea: lineas) {
            rArr.add(linea.getNombre());
        }

        String[] s = new String[rArr.size()];
        return rArr.toArray(s);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (ChoferSelectLineaDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement ChoferSelectLineaDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        // Create dialog using builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.title_chofer_select_linea_dialog) // Set title
                .setSingleChoiceItems( // Set items para el single choice
                        getLineaStringArray(this.getLineas()),
                        -1,
                        (DialogInterface dialog, int which) -> {
                            linea = lineas.get(which);
                        })
                .setPositiveButton( // Click en ok
                        R.string.label_chofer_accept_linea,
                        (DialogInterface dialog, int id) -> {
                                // Create activity
                                listener.onDialogPositiveClick(ChoferSelectLineaDialogFragment.this);
                        })
                .setNegativeButton( // Click on cancel
                        R.string.label_chofer_cancel_linea,
                        (DialogInterface dialog, int id) -> {
                            listener.onDialogNegativeClick(ChoferSelectLineaDialogFragment.this);
                        });

        // Create the AlertDialog object and return it
        return builder.create();
    }



}
