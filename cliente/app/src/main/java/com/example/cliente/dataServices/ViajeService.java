package com.example.cliente.dataServices;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cliente.MyApplication;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Parada;
import com.example.cliente.models.Viaje;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViajeService {


    private static LiveData<Viaje> updateViaje(String idLinea, String idViaje, JSONObject args){

        MutableLiveData<Viaje> viajeData = new MutableLiveData<>();
        final String url = MyApplication.getInstance().getBaseApiUrl() + "/api/linea/" + idLinea + "/viaje/" + idViaje;
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.PUT,
                url,
                args,
                (JSONObject response) -> {
                    viajeData.postValue(Viaje.fromJson(response));
                },
                (VolleyError error) -> {
                    viajeData.postValue(null);
                    Log.e("ViajeService", error.toString());
                }
        );
        MyApplication.getInstance().addToRequestQueue(request, "UPDATE_VIAJE");
        return viajeData;
    }

    public static LiveData<Viaje> createViaje(String idLinea){
        MutableLiveData<Viaje> viajeData = new MutableLiveData<>();
        final String url = MyApplication.getInstance().getBaseApiUrl() + "/api/linea/" + idLinea + "/viaje";
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                url,
                null,
                (JSONObject response) -> {
                    viajeData.postValue(Viaje.fromJson(response));
                },
                (VolleyError error) -> {
                    viajeData.postValue(null);
                    Log.e("ViajeService", error.toString());
                }
        );
        MyApplication.getInstance().addToRequestQueue(request, "CREATE_VIAJE");
        return viajeData;
    }

    public static LiveData<Viaje> getOneViaje(String idLinea, String idViaje) {
        MutableLiveData<Viaje> viajeData = new MutableLiveData<>();
        final String url = MyApplication.getInstance().getBaseApiUrl() + "/api/linea/" + idLinea + "/viaje/" + idViaje;
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                (JSONObject response) -> {
                    viajeData.postValue(Viaje.fromJson(response));
                },
                (VolleyError error) -> {
                    viajeData.postValue(null);
                    Log.e("ViajeService", error.toString());
                }
        );
        MyApplication.getInstance().addToRequestQueue(request, "GET_ONE_VIAJE");
        return viajeData;
    }

    public static LiveData<Viaje> cancelarViaje(String idLinea, String idViaje) {
        JSONObject json = new JSONObject();
        try {
            json.put("estaCancelado", true);
            return updateViaje(idLinea, idViaje, json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return updateViaje(idLinea, idViaje, json);
    }

    public static LiveData<Viaje> pausarViaje(String idLinea, String idViaje){
        JSONObject json = new JSONObject();
        try {
            json.put("estaPausado", true);
            return updateViaje(idLinea, idViaje, json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return updateViaje(idLinea, idViaje, json);
    }

    public static LiveData<Viaje> continuarViaje(String idLinea, String idViaje, String siguienteParadaId) {

        JSONObject json = new JSONObject();
        try {
            json.put("siguienteParada", siguienteParadaId);
            json.put("estaPausado", false);
            return updateViaje(idLinea, idViaje, json);

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }
}
