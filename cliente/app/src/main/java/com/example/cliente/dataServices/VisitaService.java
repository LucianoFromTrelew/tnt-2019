package com.example.cliente.dataServices;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cliente.MyApplication;
import com.example.cliente.models.Visita;

import org.json.JSONException;
import org.json.JSONObject;

public class VisitaService {
    public static LiveData<Visita> createVisita(String idLinea, String idViaje, String idParada) {
        MutableLiveData<Visita> visitaData = new MutableLiveData<>();
        final String url = MyApplication.getInstance().getBaseApiUrl() + "/api/linea" + idLinea + "/viaje/" + idViaje + "/visita";
        JSONObject body = new JSONObject();
        try {
            body.put("parada", idParada);
        } catch (JSONException e) {
            return null;
        }
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                url,
                body,
                (JSONObject response) -> {
                    visitaData.postValue(Visita.fromJson(response));
                },
                (VolleyError error) -> {
                    Log.e("VisitaService", error.toString());
                    visitaData.postValue(null);
                }
        );
        MyApplication.getInstance().addToRequestQueue(request, "CREATE_VISITA");
        return visitaData;
    }
}
