package com.example.cliente.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.cliente.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickPasajero(View view) {
        Intent intent = new Intent(this, SeleccionLineaActivity.class);
        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d("ImplicitIntents","No se puede manejar el Intent.");
        }
    }

    public void onClickChofer(View view) {
        Intent intent = new Intent(this, ChoferActivity.class);
        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d("ImplicitIntents","No se puede manejar el Intent.");
        }
    }
}
