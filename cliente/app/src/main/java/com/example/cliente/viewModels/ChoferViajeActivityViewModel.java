package com.example.cliente.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.cliente.dataServices.LineaService;
import com.example.cliente.dataServices.ViajeService;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Parada;
import com.example.cliente.models.Viaje;

public class ChoferViajeActivityViewModel extends ViewModel {

    LiveData<Linea> linea = null;
    LiveData<Viaje> viaje = null;
    LiveData<Parada> paradas = null;

    public LiveData<Linea> getLinea (String id){
        if (this.linea == null) {
            this.linea = LineaService.getLineaById(id);
        }

        return this.linea;
    }

    public  LiveData<Viaje> createViaje (String lineaId){
        if (this.viaje == null){
            this.viaje = ViajeService.createViaje(lineaId);
        }
        return this.viaje;
    }

    public LiveData<Viaje> pausarViaje(String idLinea, String idViaje){
        this.viaje = ViajeService.pausarViaje(idLinea, idViaje);
        return this.viaje;
    }

    public LiveData<Viaje> cancelarViaje(String idLinea, String idViaje) {
        this.viaje = ViajeService.cancelarViaje(idLinea, idViaje);
        return this.viaje;
    }
}
