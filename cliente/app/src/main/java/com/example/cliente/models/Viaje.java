package com.example.cliente.models;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class Viaje {

    public String id;
    public String timestamp;
    public Boolean estaPausado;
    public Boolean estaCancelado;
    public String lineaId;
    public Parada siguienteParada;

    public Viaje (String _id, String timestamp, Boolean estaPausado, Boolean estaCancelado, String lineaId, Parada siguienteParada) {
        this.id = _id;
        this.timestamp = timestamp;
        this.estaPausado = estaPausado;
        this.estaCancelado = estaCancelado;
        this.lineaId = lineaId;
        this.siguienteParada = siguienteParada;
    }

    public static Viaje fromJson(JSONObject json){
        try {
            return new Viaje(
                    json.getString("_id"),
                    json.getString("timestamp"),
                    json.getBoolean("estaPausado"),
                    json.getBoolean("estaCancelado"),
                    json.getString("linea"),
                    Parada.fromJson(json.getJSONObject("siguienteParada"))
            );
        } catch (JSONException e){
            return null;
        }
    }
}
