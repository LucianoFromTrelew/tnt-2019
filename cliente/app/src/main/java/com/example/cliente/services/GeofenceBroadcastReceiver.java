package com.example.cliente.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GeofenceBroadcastReceiver extends BroadcastReceiver {
    /**
     * Recibe intents entrantes.
     *
     * @param context the application context.
     * @param intent  enviado por Location Services. En este Intent provisto por Location
     *                Services (dentro de un PendingIntent) cuando se llama a addGeofences().
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // Encolar un JobIntentService.
        GeofenceTransitionsJobIntentService.enqueueWork(context, intent);
    }

}
