package com.example.cliente.services;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.cliente.MyApplication;
import com.example.cliente.models.Parada;
import com.example.cliente.models.Viaje;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.tasks.Task;

public class GeofencesService {

    private static final long GEOFENCE_EXPIRATION_IN_HOURS = 12;
    private static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS =
            GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;
    private static final float GEOFENCE_RADIUS_IN_METERS = 1609; // 1 milla, 1.6 km
    private static GeofencesService geofenceService = null;
    private Geofence geofence;
    private PendingIntent mGeofencePendingIntent;

    public static GeofencesService getInstance(){
        if (geofenceService == null) {
            geofenceService = new GeofencesService();
        }
        return geofenceService;

    }

    @SuppressLint("MissingPermission")
    public void addGeofence(Parada parada){
        Log.i("PARADA", parada.toString());
        this.geofence = new Geofence.Builder()
                .setRequestId(parada.getId())
                .setCircularRegion(
                        //parada.getPosicion().latitude,
                        -43.250469,
                        -65.306511,
                        //parada.getPosicion().longitude,
                        GEOFENCE_RADIUS_IN_METERS
                )
                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();

         MyApplication.getInstance()
                 .getGeofencingClient()
                 .addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                 .addOnCompleteListener((@NonNull Task<Void> task) -> {
                     Log.i("GEOFENCE", "El geofence ha sido registrado");
                 });

    }

    /**
     * Necesitamos construir y retornar GeofencingRequest.
     * Usamos la lista de geofences que queremos monitorear.
     * Indicamos tambien como las notificaciones de geofences se disparan inicialmente.
     */
    private GeofencingRequest getGeofencingRequest() {
        // TODO: #3: Especificar los geofences que se van a monitorear:

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // El flag INITIAL_TRIGGER_ENTER indica que el servicio de geofencing debe disparar
        // la notificacion de GEOFENCE_TRANSITION_ENTER si el usuario ya estaba dentro
        // del geofence que se esta agregando
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Agregar los geofences a monitorear por el servicio de geofencing.
        builder.addGeofence(this.geofence);

        // Devolver GeofencingRequest.
        return builder.build();
    }

    /**
     * Obtener un PendingIntent para 'adjuntar' a la solicitud de agregar/remover Geofences.
     * El Servicio de Localizacion emite el Intent dentro de este PendingIntent cuando una
     * transicion de geofences ocurre para la lista geofences monitoreados.
     *
     * @return PendingIntent para el IntentService que maneja las transiciones geofences.
     */
    private PendingIntent getGeofencePendingIntent() {
        // TODO: #4: Definir un intent para las transiciones de los geofences:
        // Si ya lo teniamos, devolverlo:
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(MyApplication.getInstance(), GeofenceBroadcastReceiver.class);
        // Usamos FLAG_UPDATE_CURRENT para reusar el intent pendiente tanto para el
        // addGeofences() y removeGeofences().
        intent.putExtra("unaCadena", "unaCadena");
        mGeofencePendingIntent = PendingIntent.getBroadcast(
                MyApplication.getInstance(),
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }
}
