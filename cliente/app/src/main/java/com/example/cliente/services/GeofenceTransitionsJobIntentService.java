package com.example.cliente.services;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.JobIntentService;
import android.text.TextUtils;
import android.util.Log;

import com.example.cliente.R;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.ArrayList;
import java.util.List;

public class GeofenceTransitionsJobIntentService extends JobIntentService {

    private static final int JOB_ID = 579;

    private static final String TAG = "GeofenceTransitionsIS";

    private static final String CHANNEL_ID = "channel_01";

    /**
     * Metodo para encolar tareas en este servicio.
     */
    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, GeofenceTransitionsJobIntentService.class, JOB_ID, intent);
    }

    /**
     * Manejar intents entrantes.
     * @param intent enviado por Location Services(dentro de PendingIntent)
     *               cuando se llama a addGeofences().
     */
    @Override
    protected void onHandleWork(Intent intent) {
        // Aca, responder a las transiciones de geofences
        // TODO: #6 Responder a las transiciones de los geofences:


        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        Log.i("GEOFENCING_EVENT", (String)intent.getExtras().get("unaCadena"));
        if (geofencingEvent.hasError()) {
            Log.e(TAG, "Error manejando la transicion geofence");
            return;
        }

        // Obtener el tipo de transicion.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Verificar si el tipo de la transicion es de nuestro interes
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Obtener los geofences que fueron lanzados.
            // Un solo evento puede disparar múltiples geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Obtener los detalles de la transiciones como cadenas.
            String geofenceTransitionDetails = getDetallesTransicionGeofence(geofenceTransition,
                    triggeringGeofences);

            // Send notification and log the transition details.
            //sendNotification(geofenceTransitionDetails);
            Log.i(TAG, geofenceTransitionDetails);
        } else {
            // Log the error.
            //Log.e(TAG, getString(R.string.geofence_transition_invalid_type, geofenceTransition));
        }
    }

    /**
     * Obtener los detalles de la transición y los retorna como un string formateado.
     *
     * @param geofenceTransition    ID de la transicion geofence.
     * @param triggeringGeofences   Geofence(s) lanzados.
     * @return                      Los detalles de la transición como String.
     */
    private String getDetallesTransicionGeofence(
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Obtener los Ids de cada geofence que fue lanzado.
        ArrayList<String> triggeringGeofencesIdsList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ",  triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }

    /**
     * Mapea los tipos de transiciones al equivalente legible para humanos.
     *
     * @param transitionType    Tipo de transicion (constante definida en Geofence)
     * @return                  Un String indicando el tipo de transicion
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return "Entrando al geofence";
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return "saliendo del geofence";
            default:
                return "Evento no reconocido";
        }
    }

    /**
     * Postear una notificacion en la barra de notificaciones cuando la transicion
     * es detectada.
     * Si el usuario hace click en la notificacion, el control pasa al MainActivity.
     */
    /*private void sendNotification(String notificationDetails) {
        // Obtener la instancia del Notification manager
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Android O necesita un Notification Channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.app_name);
            // Crear el canal para la notificacion
            NotificationChannel mChannel =
                    new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);

            // Setear el Notification Channel para el Notification Manager.
            mNotificationManager.createNotificationChannel(mChannel);
        }

        // Crear un Intent explicito que arranca el Main Activity.
        Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);

        // Construye un task stack.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

        // Agregar el Main Activity al task stack como el parent.
        stackBuilder.addParentStack(MainActivity.class);

        // Agregar el content Intent al stack.
        stackBuilder.addNextIntent(notificationIntent);

        // Obtener un PendingIntent que contenga el back stack entero.
        PendingIntent notificationPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        // Obtener un notification builder que sea compatible con versiones de la plataforma >= 4
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        // Configuraciones de la notificacion.
        builder.setSmallIcon(R.drawable.ic_launcher_foreground)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                        R.drawable.ic_launcher_foreground))
                .setColor(Color.RED)
                .setContentTitle(notificationDetails)
                .setContentText(getString(R.string.geofence_transition_notification_text))
                .setContentIntent(notificationPendingIntent);

        // Setear el Channel ID para Android O.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(CHANNEL_ID); // Channel ID
        }

        // Descartar la notificación una vez que el usuario la toque
        builder.setAutoCancel(true);

        // Emitir la notificación
        mNotificationManager.notify(0, builder.build());
    }*/


}
