package com.example.cliente.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.cliente.dataServices.LineaService;
import com.example.cliente.models.Linea;

import java.util.List;

public class SeleccionLineaActivityViewModel extends ViewModel {
    private LiveData<List<Linea>> lineas;

    public LiveData<List<Linea>> getLineas() {
        if (lineas == null) {
            this.lineas = LineaService.getLineas();
        }
        return lineas;
    }
}
