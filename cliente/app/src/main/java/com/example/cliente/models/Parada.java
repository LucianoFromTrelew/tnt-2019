package com.example.cliente.models;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

public class Parada {

    private String _id;
    private String anteriorId;
    private LatLng posicion;

    public Parada(String _id, String anterior, LatLng posicion) {
        this._id = _id;

        if (anterior == null) {
            this.anteriorId = null;
        }
        else {
            try {
                Parada pAnterior = Parada.fromJson(new JSONObject(anterior));
                this.anteriorId = pAnterior._id;

            }
            catch (JSONException e){

            }
        }

        this.posicion = posicion;
    }

    public Parada(String _id, LatLng posicion) {
        this(_id, null, posicion);
    }

    public static Parada fromJson(JSONObject json) {
        try {
            String anterior = json.getString("anterior");
            if (anterior.equals("null")) {
                anterior = null;
            }
            return new Parada(
                    json.getString("_id"),
                    anterior,
                    new LatLng(
                            json.getJSONObject("punto").getDouble("latitud"),
                            json.getJSONObject("punto").getDouble("longitud")
                    )
            );
        } catch (JSONException error) {
            return null;
        }
    }


    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getAnteriorId() {
        return anteriorId;
    }

    public void setAnteriorId(String anteriorId) {
        this.anteriorId = anteriorId;
    }

    public LatLng getPosicion() {
        return posicion;
    }

    public void setPosicion(LatLng posicion) {
        this.posicion = posicion;
    }

    @Override
    public String toString() {
        return "Parada{" +
                "_id='" + _id + '\'' +
                ", anteriorId='" + anteriorId + '\'' +
                ", posicion=" + posicion +
                '}';
    }
}
