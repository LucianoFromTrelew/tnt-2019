package com.example.cliente.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Visita {
    private String timestamp;
    private String viajeId;
    private String paradaId;

    public Visita(String timestamp, String viajeId, String paradaId) {
        this.timestamp = timestamp;
        this.viajeId = viajeId;
        this.paradaId = paradaId;
    }

    public static Visita fromJson(JSONObject json) {
        try {
            return new Visita(
                    json.getString("timestamp"),
                    json.getString("parada"),
                    json.getString("viaje")
            );
        } catch (JSONException e ) {
            return null;
        }
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getViajeId() {
        return viajeId;
    }

    public void setViajeId(String viajeId) {
        this.viajeId = viajeId;
    }

    public String getParadaId() {
        return paradaId;
    }

    public void setParadaId(String paradaId) {
        this.paradaId = paradaId;
    }
}
