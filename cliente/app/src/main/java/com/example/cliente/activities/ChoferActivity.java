package com.example.cliente.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.cliente.R;
import com.example.cliente.dataServices.ViajeService;
import com.example.cliente.databinding.ActivityChoferBinding;
import com.example.cliente.fragments.ChoferSelectLineaDialogFragment;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Viaje;
import com.example.cliente.viewModels.ChoferActivityViewModel;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class ChoferActivity extends AppCompatActivity implements ChoferSelectLineaDialogFragment.ChoferSelectLineaDialogListener {

    Linea linea;
    Viaje viaje;
    Button btnSelectLinea;
    Button btnCreateViaje;
    private ChoferActivityViewModel choferActivityViewModel;
    private ActivityChoferBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chofer);

        this.choferActivityViewModel = ViewModelProviders.of(this).get(ChoferActivityViewModel.class);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_chofer);
        this.binding.setLifecycleOwner(this);
        this.binding.setViewmodel(this.choferActivityViewModel);


        this.btnSelectLinea = findViewById(R.id.btnSeleccionarLinea);
        this.btnCreateViaje = findViewById(R.id.btnIniciarViaje);

        // TODO: refactor. This code was made only to test the Viaje service.
        if(linea == null){
            this.btnSelectLinea.setVisibility(View.VISIBLE);
            this.btnCreateViaje.setVisibility(View.GONE);
        }

        this.btnCreateViaje.setOnClickListener((View view) -> {

             ViajeService.createViaje(linea.getId())
                        .observe(this, (Viaje viaje) -> {
                            Log.i("VIAJE", viaje.lineaId);
                            //this.viaje = viaje;
                            //Log.i("ASD", this.viaje.toString());
                        });



        });





    }

    public void onClickSelectLinea(View view) {
        ChoferSelectLineaDialogFragment dialog = new ChoferSelectLineaDialogFragment();
        this.binding.pb.setVisibility(View.VISIBLE);
        //Aca hacemos la peticion
        this.choferActivityViewModel.getLineas().observe(this, (List<Linea> lineas) -> {
            this.binding.pb.setVisibility(View.GONE);
            //Aca hacemos la peticion
            dialog.setLineas(lineas);
            dialog.show(getSupportFragmentManager(), "SELECT_LINEA_DIALOG");
        });
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        ChoferSelectLineaDialogFragment choferSelectLineaDialogFragment = (ChoferSelectLineaDialogFragment) dialog;

        linea = choferSelectLineaDialogFragment.getLinea();
        Toast.makeText(this, String.format("CHOFER ACTIVITY - LÍNEA SELECCIONADA - %s",linea.getNombre()), Toast.LENGTH_SHORT).show();

        // Crear Intent para Ir a la activity de Monitoreo del viaje
        Intent intent = new Intent(this, ChoferViajeActivity.class);

        intent.putExtra("EXTRA_LINEA", linea.getId());

        startActivity(intent);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        linea = null;
        Toast.makeText(this, "CHOFER ACTIVITY - CANCELADO", Toast.LENGTH_SHORT).show();
    }

}
