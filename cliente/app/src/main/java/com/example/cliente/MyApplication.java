package com.example.cliente;

import android.app.Application;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class MyApplication extends Application {
    //Declare a private RequestQueue variable
    private GeofencingClient geofencingClient;
    private static String apiBaseUrl = null;
    private RequestQueue requestQueue;
    private static MyApplication mInstance;

    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
    public static synchronized MyApplication getInstance() {
        return mInstance;
    }
    /*
    Create a getRequestQueue() method to return the instance of
    RequestQueue.This kind of implementation ensures that
    the variable is instatiated only once and the same
    instance is used throughout the application
    */
    public RequestQueue getRequestQueue() {
        if (requestQueue == null)
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        return requestQueue;
    }
    /*
    public method to add the Request to the the single
    instance of RequestQueue created above.Setting a tag to every
    request helps in grouping them. Tags act as identifier
    for requests and can be used while cancelling them
    */
    public void addToRequestQueue(Request request, String tag) {
        request.setTag(tag);
        getRequestQueue().add(request);
    }
    /*
    Cancel all the requests matching with the given tag
    */
    public void cancelAllRequests(String tag) {
        getRequestQueue().cancelAll(tag);
    }

    public String getBaseApiUrl() {
        
        if (apiBaseUrl == null) {
            try {
                InputStream inputStream = getAssets().open("api.json");
                int size = inputStream.available();
                byte[] buffer = new byte[size];
                inputStream.read(buffer);
                inputStream.close();
                String json = new String(buffer, "UTF-8");
                JSONObject jsonObject = new JSONObject(json);
                apiBaseUrl = jsonObject.getString("apiBaseUrl");
            } catch (IOException | JSONException e) {
                Log.e("JUAZA", "Se rompio todo");
            }
        }
        return apiBaseUrl;
    }

    public GeofencingClient getGeofencingClient() {
        if (this.geofencingClient == null) {
            this.geofencingClient = LocationServices.getGeofencingClient(this);
        }
        return this.geofencingClient;
    }

    /*public void addGeofence(){
        new Geofence.Builder()
                // ID del geofence --> string para identificarlo
                .setRequestId(entry.getKey())
                // region circular del geofence.
                .setCircularRegion(
                        entry.getValue().latitude,
                        entry.getValue().longitude,
                        GEOFENCE_RADIUS_IN_METERS
                )
                // tiempo de expiracion del geofence.
                // se remueve automaticamente luego de este periodo de tiempo
                .setExpirationDuration(GEOFENCE_EXPIRATION_IN_MILLISECONDS)
                // configurar los tipos de transicion que se desean monitorear
                // las alertas se generan para estas transiciones.
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                // Crear el geofence.
                .build()
    }*/


}
