package com.example.cliente.activities;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.cliente.R;
import com.example.cliente.adapters.LineaAdapter;
import com.example.cliente.databinding.ActivitySeleccionLineaBinding;
import com.example.cliente.models.Linea;
import com.example.cliente.viewModels.SeleccionLineaActivityViewModel;

import java.util.List;

public class SeleccionLineaActivity extends AppCompatActivity {

    private SeleccionLineaActivityViewModel seleccionLineaViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccion_linea);


        this.seleccionLineaViewModel = ViewModelProviders.of(this).get(SeleccionLineaActivityViewModel.class);
        // La clase "ActivitySeleccionLineaBinding" la genera Android automáticamente
        // El nombre sale de => "Activity" + [NOMBRE_ACTIVITY] + "Binding"
        ActivitySeleccionLineaBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_seleccion_linea);
        binding.setLifecycleOwner(this);
        // binding tiene este atributo porque definimos en la sección de <data>
        // que tiene una variable con nombre "viewmodel"
        binding.setViewmodel(this.seleccionLineaViewModel);

        this.seleccionLineaViewModel.getLineas().observe(this, (List<Linea> lineas) -> {
            if (lineas != null) {
                // Los elementos de interfaz declarados en el XML del layout
                // se pueden manipular como atributos en el binding
                // (referenciándolos con el id que tienen en el layout)
                binding.pb.setVisibility(View.GONE);
                LineaAdapter lineaAdapter = new LineaAdapter(this, lineas);
                lineaAdapter.onLineaClicked().observe(this, (Linea linea) -> {
                    this.onClickLinea(linea);
                });
                binding.lineasList.setAdapter(lineaAdapter);
            } else {
                Log.w("SeleccionLineaActivity", "Lineas es null");
            }
        });
    }

    public void onClickLinea(Linea linea) {
        Intent intent = new Intent(this, MapsActivity.class);

        intent.putExtra("lineaId", linea.getId()); //recuperar el id de la linea y pasarla por extras

        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else{
            Log.d("ImplicitIntents","No se puede manejar el Intent.");
        }
    }


}
