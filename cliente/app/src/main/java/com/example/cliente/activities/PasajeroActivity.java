package com.example.cliente.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.cliente.fragments.ChoferSelectLineaDialogFragment;
import com.example.cliente.R;
import com.example.cliente.fragments.PasajeroSelectLineaFragment;

public class PasajeroActivity extends AppCompatActivity {

    String linea;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pasajero);
    }


    public void onClickSelectLineaPasajero(View view) {
        PasajeroSelectLineaFragment dialog = new PasajeroSelectLineaFragment();

        dialog.show(getSupportFragmentManager(), "Hola");
    }
}
