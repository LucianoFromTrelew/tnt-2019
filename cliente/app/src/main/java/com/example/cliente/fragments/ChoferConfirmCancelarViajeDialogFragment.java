package com.example.cliente.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

public class ChoferConfirmCancelarViajeDialogFragment extends DialogFragment {

    public interface ChoferConfirmCancelarViajeDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    ChoferConfirmCancelarViajeDialogListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (ChoferConfirmCancelarViajeDialogListener) context;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement ChoferConfirmCancelarViajeCiajeListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Cancelar viaje")
                .setMessage("¿Está seguro que desea cancelar el viaje?")
                .setPositiveButton(
                        "Sí",
                        (DialogInterface dialog, int id) -> {
                            listener.onDialogPositiveClick(ChoferConfirmCancelarViajeDialogFragment.this);
                        })
                .setNegativeButton(
                        "No",
                        (DialogInterface dialog, int id) -> {
                            listener.onDialogNegativeClick(ChoferConfirmCancelarViajeDialogFragment.this);
                        });

        return builder.create();
    }
}
