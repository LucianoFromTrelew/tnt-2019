package com.example.cliente.adapters;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.example.cliente.R;
import com.example.cliente.models.Linea;

import java.util.List;

public class LineaAdapter extends BaseAdapter {
    private Context context;
    private List<Linea> listItems;
    private MutableLiveData<Linea> lineaClicked;

    public LineaAdapter(Context context, List<Linea> listItems) {
        this.context = context;
        this.listItems = listItems;
        this.lineaClicked = new MutableLiveData<>();
    }

    public LiveData<Linea> onLineaClicked() {
        return this.lineaClicked;
    }

    @Override
    public int getCount() {
        return listItems.size();
    }

    @Override
    public Object getItem(int position) {
        return listItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Linea l = (Linea) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.display_lineas, null);
        Button btn = (Button) convertView.findViewById(R.id.lineasTitles);
        btn.setOnClickListener((View v) -> {
            this.lineaClicked.setValue(this.listItems.get(position));
        });
        btn.setText("Linea " + String.valueOf(l.getNombre()));

        return convertView;
    }
}
