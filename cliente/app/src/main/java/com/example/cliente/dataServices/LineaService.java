package com.example.cliente.dataServices;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.cliente.MyApplication;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Parada;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LineaService {
    public static LiveData<List<Linea>> getLineas() {
        MutableLiveData<List<Linea>> lineaData = new MutableLiveData<>();
        final String url = MyApplication.getInstance().getBaseApiUrl() + "/api/linea";
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                (JSONArray response) -> {
                    try {
                        List<Linea> lineas = new ArrayList<>();
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject lineaJson = response.getJSONObject(i);
                            lineas.add(Linea.fromJson(lineaJson));
                        }
                        lineaData.postValue(lineas);
                    } catch (JSONException error) {
                        lineaData.postValue(null);
                        Log.e("LineaService", error.toString());
                    }

                },
                (VolleyError error) -> {
                    lineaData.postValue(null);
                    Log.e("LineaService", error.toString());
                }
        );
        MyApplication.getInstance().addToRequestQueue(request, "GET_LINEAS");
        return lineaData;
    }

    public static LiveData<Linea> getLineaById(String id) {
        MutableLiveData<Linea> lineaData = new MutableLiveData<>();
        final String url = MyApplication.getInstance().getBaseApiUrl() + "/api/linea/" + id;
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                url,
                null,
                (JSONObject response) -> {
                    try {

                        JSONObject lineaJson = response.getJSONObject("linea");
                        JSONArray paradasJson = response.getJSONArray("paradas");

                        List<Parada> paradas = new ArrayList<>();
                        for (int i = 0; i < paradasJson.length(); i++) {
                            paradas.add(Parada.fromJson(paradasJson.getJSONObject(i)));
                        }

                        Linea linea = Linea.fromJson(lineaJson);
                        linea.setParadas(paradas);
                        lineaData.postValue(linea);

                    } catch (JSONException error) {
                        lineaData.postValue(null);
                        Log.e("LineaService", error.toString());
                    }
                },
                (VolleyError error) -> {
                    lineaData.postValue(null);
                    Log.e("LineaService", error.toString());
                }
        );
        MyApplication.getInstance().addToRequestQueue(request, "GET_LINEA_BY_ID");
        return lineaData;
    }
}
