package com.example.cliente.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cliente.R;
import com.example.cliente.models.Linea;
import com.example.cliente.models.Parada;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChoferSelectSiguienteParadaDialogFragment extends DialogFragment {

    private Parada parada;
    private List<Parada> paradas;
    ChoferSelectSiguienteParadaDialogListener listener;

    public interface ChoferSelectSiguienteParadaDialogListener {
        public void onDialogSiguienteParadaClick(DialogFragment dialog);
        public void onDialogCancelarClick(DialogFragment dialog);
    }


    public Parada getParada(){
        return this.parada;
    }

    public void setParadas(List<Parada> paradas) {
        this.paradas = paradas;
    }


    public ChoferSelectSiguienteParadaDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (ChoferSelectSiguienteParadaDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement ChoferSelectLineaDialogListener");
        }
    }


    private String[] getParadasStringArray(List<Parada> paradas) {
        ArrayList<String> rArr = new ArrayList<String>();

        for (Parada parada: paradas) {
            rArr.add(parada.toString());
        }

        String[] s = new String[rArr.size()];
        return rArr.toArray(s);
    }

    public List<Parada> getParadas() {
        return paradas;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        // Create dialog using builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Seleccione la parada") // Set title
                .setSingleChoiceItems( // Set items para el single choice
                        getParadasStringArray(this.getParadas()),
                        -1,
                        (DialogInterface dialog, int which) -> {
                            parada = paradas.get(which);
                        })
                .setPositiveButton( // Click en ok
                        R.string.label_chofer_accept_linea,
                        (DialogInterface dialog, int id) -> {
                            // Create activity
                            listener.onDialogSiguienteParadaClick(ChoferSelectSiguienteParadaDialogFragment.this);
                        })
                .setNegativeButton( // Click on cancel
                        R.string.label_chofer_cancel_linea,
                        (DialogInterface dialog, int id) -> {
                            listener.onDialogCancelarClick(ChoferSelectSiguienteParadaDialogFragment.this);
                        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

}
