package com.example.cliente.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Linea implements Serializable{

    private String _id;
    private String nombre;
    private boolean estaActiva;
    private List<Parada> paradas;

    public Linea(String _id, String nombre, boolean estaActiva) {
        this._id = _id;
        this.nombre = nombre;
        this.estaActiva = estaActiva;
        this.paradas = new ArrayList<>();
    }

    public Linea(String _id, String nombre, boolean estaActiva, List<Parada> paradas) {
        this(_id, nombre, estaActiva);
        this.paradas = paradas;
    }

    public static Linea fromJson(JSONObject json) {
        try {
            return new Linea(
                    json.getString("_id"),
                    json.getString("nombre"),
                    json.getBoolean("estaActiva")
            );
        } catch (JSONException error) {
            return null;
        }
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEstaActiva() {
        return estaActiva;
    }

    public void setEstaActiva(boolean estaActiva) {
        this.estaActiva = estaActiva;
    }

    public List<Parada> getParadas() {
        return paradas;
    }

    public void setParadas(List<Parada> paradas) {
        this.paradas = paradas;
    }

    @Override
    public String toString() {
        return "Linea{" +
                "_id='" + _id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", estaActiva=" + estaActiva +
                '}';
    }

}
