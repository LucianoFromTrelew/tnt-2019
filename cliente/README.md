## TNT-2019

### Proyecto Android

Para correr el proyecto, primero se debe agregar la _API Key_ de Google Maps, creando el archivo `app/src/main/res/values/google_maps_api.xml` con el siguiente contenido:

```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <string name="google_maps_api" templateMergeStrategy="preserve" translatable="false">TU_API_KEY_VA_ACÁ</string>
</resources>
```

Luego, crear el directorio `assets` en `app/src/main` y crear dentro el archivo `api.json`, con el siguiente contenido:

```
{
    "apiBaseUrl": "DIRECCION_IP_DE_LA_API"
}
```
