import mongoose from "mongoose";
import "dotenv/config";
import { getDbUri } from "./src/config/database";
import Punto from "./src/models/punto.model";
import Parada from "./src/models/parada.model";
import Linea from "./src/models/linea.model";
import faker from "faker";

(async () => {
  try {
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });
    await Punto.deleteMany({});
    await Linea.deleteMany({});
    await Parada.deleteMany({});

    const mPuntos = require("./data/puntos");
    const mLineas = require("./data/lineas");

    await Punto.insertMany(mPuntos);
    await Linea.insertMany(mLineas);

    const lineas = await Linea.find();
    const puntos = await Punto.find();

    for (const linea of lineas) {
      for (let index = 0; index < 5; index++) {
        const punto =
          puntos[faker.random.number({ min: 0, max: puntos.length })];
        await Parada.create({
          linea: linea._id,
          punto: punto._id
        });
      }
    }

    for (const linea of lineas) {
      const paradas = await linea.getParadas();
      for (let index = 1; index < paradas.length; index++) {
        const parada = paradas[index];
        parada.anterior = paradas[index - 1];
        await parada.save();
      }
    }

    console.log("Datos cargados correctamente!");
  } catch (error) {
    console.log("*** PINCHOSE ***");
    console.log(error);

    await Punto.deleteMany({});
    await Linea.deleteMany({});
    await Parada.deleteMany({});
  } finally {
    await mongoose.disconnect();
  }
})();
