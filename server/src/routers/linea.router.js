import { Router } from "express";
import {
  createLinea,
  getLineaById,
  getLineas,
  getLineaStatistics,
  getOneLinea
} from "../controllers/linea.controller";
import {
  createViaje,
  getOneViaje,
  getViajeById,
  updateViaje
} from "../controllers/viaje.controller";
import { getParadaFromBody } from "../controllers/parada.controller";
import { createVisita } from "../controllers/visita.controller";

const router = Router();

router
  .get("/", getLineas)
  .post("/", createLinea)
  .get("/:lineaId", getLineaById, getOneLinea)
  .get("/:lineaId/estadisticas", getLineaStatistics)
  .post("/:lineaId/viaje", getLineaById, createViaje)
  .get("/:lineaId/viaje/:viajeId", getLineaById, getViajeById, getOneViaje)
  .put("/:lineaId/viaje/:viajeId", getLineaById, getViajeById, updateViaje)
  .post(
    "/:lineaId/viaje/:viajeId/visita",
    getLineaById,
    getViajeById,
    getParadaFromBody,
    createVisita
  );

export default router;
