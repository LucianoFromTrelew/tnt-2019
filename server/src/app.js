import express from "express";
import cors from "cors";
import morgan from "morgan";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import "dotenv/config";

import { getDbUri } from "./config/database";
import LineaRouter from "./routers/linea.router";

import Linea from "./models/linea.model";
import Parada from "./models/parada.model";
import Punto from "./models/punto.model";
import Viaje from "./models/viaje.model";
import Visita from "./models/visita.model";

const getApp = async () => {
  try {
    const app = express();

    // CORS
    app.use(cors());

    // Logger
    if (process.env.NODE_ENV !== "test") app.use(morgan("combined"));

    // Body parser
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // Routers
    app.get("/", (request, response, next) => {
      response.send({ msg: "Hello world!" });
    });
    app.use("/api/linea", LineaRouter);

    // Conectarse a la BD
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });
    return app;
  } catch (error) {
    throw new Error(error);
  }
};

export default getApp;
