import Viaje from "../models/viaje.model";

export const createViaje = async (request, response, next) => {
  try {
    const { linea } = request;
    const viaje = await new Viaje({ linea: linea._id }).save();
    response.json(viaje);
  } catch (error) {
    response.status(400).json({ msg: `No se pudo crear el viaje [${error}]` });
  }
};

export const getViajeById = async (request, response, next) => {
  // Middleware
  const { viajeId } = request.params;
  try {
    const viaje = await Viaje.findById(viajeId);
    if (!viaje) throw new Error("Viaje no encontrado");
    request.viaje = viaje;
    next();
  } catch (error) {
    response
      .status(404)
      .json({ msg: `No se pudo encontrar el viaje [${viajeId}]` });
  }
};

export const getOneViaje = async (request, response, next) => {
  const { viaje } = request;
  const visitas = await viaje.getVisitas();
  response.json({ viaje, visitas });
};

export const updateViaje = async (request, response, next) => {
  const { viaje, linea } = request;
  // No puedo hacer viaje.update, así que hago el findByIdAndUpdate y fue
  const updatedViaje = await Viaje.findByIdAndUpdate(viaje._id, request.body, {
    new: true
  });
  response.status(200).json(updatedViaje);
};
