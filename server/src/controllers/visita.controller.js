import Visita from "../models/visita.model";

export const createVisita = async (request, response, next) => {
  const { parada, viaje } = request;
  try {
    const visita = await new Visita({
      parada: parada._id,
      viaje: viaje._id
    }).save();
    response.status(201).json(visita);
  } catch (error) {
    response.status(400).json(`No se pudo crear la visita ${error}`);
  }
};
