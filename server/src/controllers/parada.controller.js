import Parada from "../models/parada.model";

export const getParadaFromBody = async (request, response, next) => {
  // Extraemos el campo `parada` del request.body, y lo asignamos a una variable llamada `paradaId`
  const { parada: paradaId } = request.body;
  try {
    const parada = await Parada.findById(paradaId);
    if (!parada) throw new Error("Parada no encontrada");
    request.parada = parada;
    next();
  } catch (error) {
    response
      .status(404)
      .json({ msg: `No se pudo encontrar la parada [${paradaId}] - ${error}` });
  }
};
