import Linea from "../models/linea.model";
import { wait } from "../utils/time";

export const getLineas = async (request, response, next) => {
  await wait(2000);
  response.send(await Linea.find({ estaActiva: true }));
};

export const getLineaById = async (request, response, next) => {
  // Esta función pasa a ser de middleware nomas
  const { lineaId } = request.params;
  try {
    const linea = await Linea.findById(lineaId);
    if (!linea) throw new Error("Línea no encontrada");
    request.linea = linea;
    next();
  } catch (error) {
    response
      .status(404)
      .json({ msg: `No se pudo encontrar la línea [${lineaId}]` });
  }
};

export const getOneLinea = async (request, response, next) => {
  const { linea } = request;
  const paradas = await linea.getParadas(); 
  const viaje = await linea.getViajeActual();
  response.json({ linea, paradas, viaje});
};

export const getLineaStatistics = async (request, response, next) => {};

export const createLinea = async (request, response, next) => {
  const linea = new Linea(request.body);
  try {
    await linea.save();
    response.send({ msg: "Línea creada correctamente" });
  } catch (error) {
    response.status(400).send({ msg: "No se pudo crear la línea" });
  }
};
