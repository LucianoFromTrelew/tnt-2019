import getApp from "./app";
import "dotenv/config";

const run = async () => {
  const { EXPRESS_PORT } = process.env;
  try {
    const app = await getApp();
    app.listen(EXPRESS_PORT, () => {
      console.log(`[EXPRESS] Listen on port ${EXPRESS_PORT}`);
    });
  } catch (error) {
    console.log("se rompio");
    console.log(error);
  }
};

run();
