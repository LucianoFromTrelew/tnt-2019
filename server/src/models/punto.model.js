import mongoose from "mongoose";

const puntoSchema = new mongoose.Schema({
  latitud: {
    type: Number,
    required: true
  },
  longitud: {
    type: Number,
    required: true
  }
});

class PuntoClass {}

puntoSchema.loadClass(PuntoClass);

const Punto = mongoose.model("Punto", puntoSchema);

export default Punto;
