import Linea from "./linea.model";

import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
import Visita from "./visita.model";

const viajeSchema = new mongoose.Schema({
  timestamp: {
    type: Date,
    default: Date.now
  },
  timestampFin: {
    type: Date,
    default: null
  },
  estaPausado: {
    type: mongoose.Schema.Types.Boolean,
    default: false
  },
  estaCancelado: {
    type: mongoose.Schema.Types.Boolean,
    default: false
  },
  linea: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Linea",
    required: true,
    autopopulate: true
  },
  siguienteParada: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Parada",
    default: null,
    autopopulate: true
  }
});

viajeSchema.plugin(autopopulate);

class ViajeClass {
  async finalizar() {
    await Viaje.findByIdAndUpdate(this._id, { timestampFin: Date.now() });
    const lineaPuedeDesactivarse = await this.linea.puedeDesactivarse();
    if (lineaPuedeDesactivarse) {
      await this.linea.desactivar();
    }
  }
  async updateSiguienteParada() {
    const siguienteParada = await this.linea.getSiguienteParada(
      this.siguienteParada
    );
    if (!siguienteParada) {
      await this.finalizar();
    }
    await Viaje.findByIdAndUpdate(this._id, { siguienteParada });
  }

  async getVisitas() {
    const visitas = await Visita.find({ viaje: this._id });
    return visitas;
  }
}

viajeSchema.loadClass(ViajeClass);

viajeSchema.pre("save", async function() {
  const linea = await Linea.findById(this.linea._id);
  const paradas = await linea.getParadas();
  if (paradas.length <= 0) {
    throw new Error("No se puede crear un viaje si la línea no tiene paradas");
  }
  await linea.activar();
  // Seteamos la primer parada de la linea como la siguiente (la primera) del viaje que se quiere crear
  this.siguienteParada = paradas[0];
});

const Viaje = mongoose.model("Viaje", viajeSchema);

export default Viaje;
