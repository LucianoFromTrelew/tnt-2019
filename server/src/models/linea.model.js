import Viaje from "./viaje.model";

import mongoose from "mongoose";
import Parada from "./parada.model";

// Con el esquema seguimos definiendo la estructura de datos
const lineaSchema = new mongoose.Schema({
  nombre: {
    type: String,
    unique: true,
    required: true
  },
  estaActiva: {
    type: Boolean,
    default: true
  }
});

// Esta clase puede tener setter/getterss, metodos de instancia, metodos estáticos...
class LineaClass {
  async puedeDesactivarse() {
    const cantidadViajesActivos = await Viaje.find({ linea: this._id })
      .where("timestampFin")
      .equals(null)
      .countDocuments();
    return !(cantidadViajesActivos > 0);
  }
  async desactivar() {
    await Linea.findByIdAndUpdate(this._id, { estaActiva: false });
  }

  async activar() {
    await Linea.findByIdAndUpdate(this._id, { estaActiva: true });
  }

  async getViajes() {
    const viajes = await Viaje.find({ linea: this._id });
    return viajes;
  }

  async getViajeActual(){
    const viaje = await Viaje.findOne({linea: this._id, timestampFin: null});
    return viaje;
  }

  async getParadas() {
    let paradas = await Parada.find({ linea: this._id }, "anterior _id punto");
    for (let parada of paradas) {
      if (parada.anterior != null) {
        parada.anterior = parada.anterior._id;
      }
    }
    return paradas;
  }

  async getSiguienteParada(parada) {
    const paradas = await this.getParadas();
    try {
      const siguienteParadaIndex =
        paradas.findIndex(p => p.id === parada.id) + 1;
      if (!siguienteParadaIndex) return null;
      const siguienteParada = paradas[siguienteParadaIndex];
      return siguienteParada;
    } catch (error) {
      return null;
    }
  }
}

//  Cargamos la clase en el esquema
lineaSchema.loadClass(LineaClass);

// Ahora hay que crear el model a partir de mongoose
// No podemos crear el modelo a partir del esquema (lineaSchema)
const Linea = mongoose.model("Linea", lineaSchema);

export default Linea;
