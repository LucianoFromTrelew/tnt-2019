import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";

const paradaSchema = new mongoose.Schema({
  linea: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Linea",
    required: true,
    autopopulate: true
  },
  punto: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Punto",
    required: true,
    autopopulate: true
  },
  anterior: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Parada",
    default: null,
    autopopulate: true
  }
});

paradaSchema.plugin(autopopulate);
class ParadaClass {
  async esUltima() {
    const paradas = await Parada.find({ anterior: this._id });
    return paradas.length <= 0;
  }
  esPrimera() {
    return !this.anterior;
  }
}

paradaSchema.loadClass(ParadaClass);

const Parada = mongoose.model("Parada", paradaSchema);

export default Parada;
