import mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";
import Viaje from "./viaje.model";

const visitaSchema = new mongoose.Schema({
  timestamp: {
    type: Date,
    default: Date.now
  },
  viaje: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Viaje",
    required: true,
    autopopulate: true
  },
  parada: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Parada",
    required: true,
    autopopulate: true
  }
});

visitaSchema.plugin(autopopulate);

class VisitaClass {}

visitaSchema.loadClass(VisitaClass);

visitaSchema.pre("save", async function() {
  const viaje = await Viaje.findById(this.viaje._id);
  await viaje.updateSiguienteParada();
});

const Visita = mongoose.model("Visita", visitaSchema);

export default Visita;
