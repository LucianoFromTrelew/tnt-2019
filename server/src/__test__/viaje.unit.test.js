import Viaje from "../models/viaje.model";
import Linea from "../models/linea.model";
import Punto from "../models/punto.model";
import Parada from "../models/parada.model";

import mongoose from "mongoose";
import "dotenv/config";
import { getDbUri } from "../config/database";
import Visita from "../models/visita.model";

describe("Viaje", () => {
  let linea, viaje, parada, segundaParada;

  beforeEach(async () => {
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });
  });

  beforeEach(async () => {
    linea = await new Linea({ nombre: "La linea" }).save();
    const punto = await new Punto({ latitud: 12, longitud: 21 }).save();
    parada = await new Parada({ linea: linea._id, punto: punto._id }).save();
    segundaParada = await new Parada({
      linea: linea._id,
      punto: punto._id
    }).save();
    viaje = await new Viaje({ linea: linea._id }).save();
  });

  it("should return correct Linea", async () => {
    const elViaje = await Viaje.findById(viaje._id);
    expect(elViaje.linea.nombre).toEqual(linea.nombre);
  });

  it("should return correctly siguienteParada", async () => {
    let updatedViaje = await Viaje.findById(viaje._id);
    expect(updatedViaje.siguienteParada.id).toEqual(parada.id);
    await Visita.create({
      viaje: updatedViaje._id,
      parada: updatedViaje.siguienteParada._id
    });
    updatedViaje = await Viaje.findById(viaje._id);
    expect(updatedViaje.siguienteParada.id).toEqual(segundaParada.id);
    await Visita.create({
      viaje: updatedViaje._id,
      parada: updatedViaje.siguienteParada._id
    });
    updatedViaje = await Viaje.findById(viaje._id);
    expect(updatedViaje.siguienteParada).toBeNull();
  });

  it("should not create a Viaje if Linea has not any Paradas", async done => {
    await Parada.deleteMany({});
    try {
      viaje = await new Viaje({ linea: linea._id }).save();
      done.fail("Cannot create viaje if linea has not any paradas");
    } catch (error) {
      done();
    }
  });

  it("should return Visitas from a Viaje correctly", async () => {
    await Visita.create({
      viaje: viaje._id,
      parada: parada._id
    });
    await Visita.create({
      viaje: viaje._id,
      parada: parada._id
    });
    expect((await viaje.getVisitas()).length).toBe(2);
  });

  it("should set Viaje as completed if visited last Parada", async () => {
    expect(viaje.timestampFin).toBeNull();
    await Visita.create({
      viaje: viaje._id,
      parada: viaje.siguienteParada._id
    });
    let updatedViaje = await Viaje.findById(viaje._id);
    expect(updatedViaje.timestampFin).toBeNull();
    await Visita.create({
      viaje: viaje._id,
      parada: updatedViaje.siguienteParada._id
    });
    updatedViaje = await Viaje.findById(viaje._id);
    expect(updatedViaje.timestampFin).toBeInstanceOf(Date);
  });

  afterEach(async () => {
    await Linea.deleteMany({});
    await Viaje.deleteMany({});
    await Punto.deleteMany({});
    await Parada.deleteMany({});
    await mongoose.disconnect();
  });
});
