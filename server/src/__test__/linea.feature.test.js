import request from "supertest";
import getApp from "../app";
import Linea from "../models/linea.model";
import Parada from "../models/parada.model";
import Punto from "../models/punto.model";

describe("linea", () => {
  let app;

  beforeEach(async () => {
    app = await getApp();
  });

  it("should work", () => {
    expect(1).toBe(1);
  });

  describe("GET /api/linea", () => {
    let response;
    beforeEach(async () => {
      await new Linea({ nombre: "Linea 1", estaActiva: true }).save();
      await new Linea({ nombre: "Linea 2", estaActiva: true }).save();
      await new Linea({ nombre: "Linea 3", estaActiva: false }).save();
      await new Linea({ nombre: "Linea 4", estaActiva: false }).save();
      response = await request(app).get("/api/linea");
    });

    it("should return a status code 200", async () => {
      expect(response.status).toBe(200);
    });

    it("should return an array of Linea", async () => {
      expect(response.body).toBeInstanceOf(Array);
    });

    it("should return an array of active Lineas", async () => {
      expect(response.body.length).toBe(2);
    });
  });

  describe("POST /api/linea", () => {
    let response;
    const lineaData = {
      nombre: "Linea 1"
    };
    beforeEach(async () => {
      response = await request(app)
        .post("/api/linea")
        .send(lineaData);
    });

    it("should return a status code 200", async () => {
      expect(response.status).toBe(200);
    });

    it("should create correctly", async () => {
      expect(response.body).toHaveProperty("msg", "Línea creada correctamente");
      const lineaCount = await Linea.countDocuments();
      expect(lineaCount).toBe(1);
    });
  });

  describe("GET /api/linea/:id", () => {
    const lineaData = {
      nombre: "Linea 1"
    };
    let linea, punto, primeraParada, segundaParada, response, wrongResponse;
    beforeEach(async () => {
      linea = await new Linea(lineaData).save();
      punto = await new Punto({ latitud: 12, longitud: 21 }).save();
      primeraParada = await new Parada({
        linea: linea._id,
        punto: punto._id
      }).save();
      segundaParada = await new Parada({
        linea: linea._id,
        punto: punto._id,
        anterior: primeraParada._id
      }).save();

      response = await request(app).get(`/api/linea/${linea._id}`);

      wrongResponse = await request(app).get(`/api/linea/${-1}`);
    });

    it("should return the same Linea", () => {
      expect(response.body.linea.nombre).toEqual(linea.nombre);
      expect(response.body.linea.estaActiva).toEqual(linea.estaActiva);
    });

    it("should return Paradas", async () => {
      expect(response.body.paradas.length).toBe(2);
    });

    it("should return an error if sending wrong data", () => {
      expect(wrongResponse.status).not.toBe(200);
    });
  });

  afterEach(async () => {
    await Linea.deleteMany({});
  });
});
