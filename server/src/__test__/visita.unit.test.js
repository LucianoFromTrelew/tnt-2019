import Visita from "../models/visita.model";
import Viaje from "../models/viaje.model";
import Parada from "../models/parada.model";
import Linea from "../models/linea.model";
import Punto from "../models/punto.model";

import mongoose from "mongoose";
import "dotenv/config";
import { getDbUri } from "../config/database";

describe("Visita", () => {
  let linea, punto, primeraParada, segundaParada, viaje, visita;
  beforeEach(async () => {
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });

    linea = await new Linea({ nombre: "La linea" }).save();
    punto = await new Punto({ latitud: 12, longitud: 21 }).save();
    primeraParada = await new Parada({
      linea: linea._id,
      punto: punto._id
    }).save();
    segundaParada = await new Parada({
      linea: linea._id,
      punto: punto._id
    }).save();
    viaje = await new Viaje({ linea: linea._id }).save();
  });

  it("should not allow to save a Visita without Viaje or Parada references", async done => {
    visita = new Visita();
    try {
      await visita.save();
      done.fail(
        "Should not allow to save a Visita without Viaje or Parada references"
      );
    } catch (error) {
      done();
    }
  });

  it("should create Visita correctly", async () => {
    visita = await new Visita({
      viaje: viaje._id,
      parada: primeraParada._id
    }).save();
    expect(visita._id).toBeDefined();
    const laVisita = await Visita.findById(visita._id);
    expect(laVisita.parada._id).toEqual(primeraParada._id);
    expect(laVisita.viaje._id).toEqual(viaje._id);
  });

  it("should update Viaje's siguienteParada", async () => {
    expect(viaje.siguienteParada._id).toEqual(primeraParada._id);
    visita = await new Visita({
      viaje: viaje._id,
      parada: primeraParada._id
    }).save();
    const updatedViaje = await Viaje.findById(viaje._id);
    expect(updatedViaje.siguienteParada._id).toEqual(segundaParada._id);
  });

  afterEach(async () => {
    await Parada.deleteMany({});
    await Linea.deleteMany({});
    await Punto.deleteMany({});
    await Viaje.deleteMany({});
    await Visita.deleteMany({});
    await mongoose.disconnect();
  });
});
