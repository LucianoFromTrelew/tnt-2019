import request from "supertest";
import getApp from "../app";
import Linea from "../models/linea.model";
import Viaje from "../models/viaje.model";
import Punto from "../models/punto.model";
import Parada from "../models/parada.model";
import Visita from "../models/visita.model";

describe("visita", () => {
  let app;

  beforeEach(async () => {
    app = await getApp();
  });

  it("should work", () => {
    expect(1).toBe(1);
  });

  describe("POST /api/linea/{idLinea}/viaje/{idViaje}/visita", () => {
    let linea,
      punto,
      parada,
      viaje,
      visita,
      response,
      wrongResponseMissingData,
      wrongResponseWrongData;

    beforeEach(async () => {
      linea = await new Linea({ nombre: "La linea" }).save();
      punto = await new Punto({ latitud: 12, longitud: 21 }).save();
      parada = await new Parada({ linea: linea._id, punto: punto._id }).save();
      viaje = await new Viaje({ linea: linea._id }).save();
      response = await request(app)
        .post(`/api/linea/${linea._id}/viaje/${viaje._id}/visita`)
        .send({ parada: parada._id });
      wrongResponseMissingData = await request(app)
        .post(`/api/linea/${linea._id}/viaje/${viaje._id}/visita`)
        .send();
      wrongResponseWrongData = await request(app)
        .post(`/api/linea/${linea._id}/viaje/${viaje._id}/visita`)
        .send({ parada: -123 });
    });

    it("should create a Visita correctly", () => {
      expect(response.status).toBe(201);
    });

    it("should not create a Visita if missing data", () => {
      expect(wrongResponseMissingData.status).toBe(404);
    });

    it("should not create a Visita if sending wrong data", () => {
      expect(wrongResponseWrongData.status).toBe(404);
    });
  });

  afterEach(async () => {
    await Linea.deleteMany({});
    await Punto.deleteMany({});
    await Parada.deleteMany({});
    await Viaje.deleteMany({});
    await Visita.deleteMany({});
  });
});
