import Punto from "../models/punto.model";

import mongoose from "mongoose";
import "dotenv/config";
import { getDbUri } from "../config/database";

describe("Punto", () => {
  beforeEach(async () => {
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });
  });

  it("should not allow to save an empty Punto", async done => {
    const punto = new Punto();
    try {
      await punto.save();
      done.fail("Should not allow to save an empty Punto");
    } catch (error) {
      done();
    }
  });

  afterEach(async () => {
    await Punto.deleteMany({});
    await mongoose.disconnect();
  });
});
