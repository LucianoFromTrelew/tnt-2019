import Linea from "../models/linea.model";
import Viaje from "../models/viaje.model";

import mongoose from "mongoose";
import "dotenv/config";
import { getDbUri } from "../config/database";
import Parada from "../models/parada.model";
import Punto from "../models/punto.model";

describe("Linea", () => {
  let linea, viaje, otroViaje, primeraParada, segundaParada, punto;

  beforeEach(async () => {
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });
    linea = await new Linea({ nombre: "La linea" }).save();
    punto = await new Punto({ latitud: 12, longitud: 21 }).save();
    primeraParada = await new Parada({
      linea: linea._id,
      punto: punto._id
    }).save();
    segundaParada = await new Parada({
      linea: linea._id,
      punto: punto._id,
      anterior: primeraParada._id
    }).save();
    viaje = await new Viaje({ linea: linea._id }).save();
    otroViaje = await new Viaje({ linea: linea._id }).save();
  });

  it("should return Paradas", async () => {
    const paradas = await linea.getParadas();
    // Sacamos los atributos que nos interesan
    const paradaMapper = ({ id }) => ({ id });
    expect(paradas.length).toBe(2);
    expect(paradas).toMatchObject(
      [primeraParada, segundaParada].map(paradaMapper)
    );
  });

  it("should return Viajes", async () => {
    // Sacamos las propiedades que nos interesan
    const viajeMapper = ({ _id, estaPausado, estaCancelado }) => ({
      _id,
      estaCancelado,
      estaPausado
    });
    const viajes = await linea.getViajes();
    expect(viajes.map(viajeMapper)).toMatchObject(
      [viaje, otroViaje].map(viajeMapper)
    );
  });

  it("should indicate correctly if a Linea can be deactivated", async () => {
    expect(await linea.puedeDesactivarse()).toBeFalsy();
    const viajes = await linea.getViajes();
    for (const viaje of viajes) {
      await viaje.finalizar();
    }
    expect(await linea.puedeDesactivarse()).toBeTruthy();
  });

  it("should be disabled if it has not active Viajes", async () => {
    expect(linea.estaActiva).toBeTruthy();
    const viajes = await linea.getViajes();
    for (const viaje of viajes) {
      await viaje.finalizar();
    }
    const updatedLinea = await Linea.findById(linea._id);
    expect(updatedLinea.estaActiva).toBeFalsy();
  });

  it("should be enabled if ti has actives Viajes", async () => {
    await linea.desactivar();
    let updatedLinea = await Linea.findById(linea._id);
    expect(updatedLinea.estaActiva).toBeFalsy();
    await Viaje.create({
      linea: linea._id
    });
    updatedLinea = await Linea.findById(linea._id);
    expect(updatedLinea.estaActiva).toBeTruthy();
  });

  afterEach(async () => {
    await Linea.deleteMany({});
    await Viaje.deleteMany({});
    await Punto.deleteMany({});
    await Parada.deleteMany({});
    await mongoose.disconnect();
  });
});
