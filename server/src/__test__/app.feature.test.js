import request from "supertest";
import getApp from "../app";

describe("app", () => {
  let app;

  beforeEach(async () => {
    app = await getApp();
  });

  it("should work", () => {
    expect(1).toBe(1);
  });

  describe("/ route", () => {
    let response;
    beforeEach(async () => {
      response = await request(app).get("/");
    });

    it("should return a status code 200", async () => {
      expect(response.status).toBe(200);
    });

    it("should return a 'hello world' message", async () => {
      expect(response.body).toHaveProperty("msg", "Hello world!");
    });
  });
});
