import Parada from "../models/parada.model";
import Linea from "../models/linea.model";
import Punto from "../models/punto.model";

import mongoose from "mongoose";
import "dotenv/config";
import { getDbUri } from "../config/database";

describe("Parada", () => {
  let linea, punto;
  beforeEach(async () => {
    await mongoose.connect(getDbUri(), { useNewUrlParser: true });
    linea = await new Linea({ nombre: "La linea" }).save();
    punto = await new Punto({ latitud: 12, longitud: 21 }).save();
  });

  it("should not allow to save a Parada without Linea or Punto references", async done => {
    const parada = new Parada();
    try {
      await parada.save();
      done.fail(
        "should not allow to save a Parada without Linea or Punto references"
      );
    } catch (error) {
      done();
    }
  });

  it("should create a Parada correctly", async () => {
    await new Parada({
      linea: linea._id,
      punto: punto._id
    }).save();

    const parada = await Parada.findOne();
    expect(parada.linea.nombre).toEqual(linea.nombre);
    expect(parada.punto.latitud).toEqual(punto.latitud);
    expect(parada.punto.longitud).toEqual(punto.longitud);
  });

  describe("esPrimera and esUltima", () => {
    let primeraParada, segundaParada;

    beforeEach(async () => {
      primeraParada = await new Parada({
        linea: linea._id,
        punto: punto._id
      }).save();
      segundaParada = await new Parada({
        linea: linea._id,
        punto: punto._id,
        anterior: primeraParada._id
      }).save();
    });

    it("should indicate correctly which Parada is last", async () => {
      expect(await primeraParada.esUltima()).toBeFalsy();
      expect(await segundaParada.esUltima()).toBeTruthy();
    });

    it("should indicate correctly which Parada is first", () => {
      expect(primeraParada.esPrimera()).toBeTruthy();
      expect(segundaParada.esPrimera()).toBeFalsy();
    });
  });

  afterEach(async () => {
    await Parada.deleteMany({});
    await Linea.deleteMany({});
    await Punto.deleteMany({});
    await mongoose.disconnect();
  });
});
