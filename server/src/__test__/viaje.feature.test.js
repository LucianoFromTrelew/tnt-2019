import request from "supertest";
import getApp from "../app";
import Linea from "../models/linea.model";
import Viaje from "../models/viaje.model";
import Punto from "../models/punto.model";
import Parada from "../models/parada.model";
import Visita from "../models/visita.model";

describe("viaje", () => {
  let app;

  beforeEach(async () => {
    app = await getApp();
  });

  it("should work", () => {
    expect(1).toBe(1);
  });

  describe("POST /api/linea/{id}/viaje", () => {
    let linea, punto, parada, response, wrongResponse;
    beforeEach(async () => {
      punto = await new Punto({ latitud: 12, longitud: 21 }).save();
      linea = await new Linea({ nombre: "La linea" }).save();
      parada = await new Parada({ linea: linea._id, punto: punto._id }).save();
      response = await request(app)
        .post(`/api/linea/${linea._id}/viaje`)
        .send();
      wrongResponse = await request(app)
        .post(`/api/linea/-123/viaje`)
        .send();
    });

    it("should create correctly a new Viaje", () => {
      expect(response.status).toBe(200);
      expect(response.body.linea).toEqual(linea.id);
    });

    it("should not create a new Viaje if Linea does not exist", () => {
      expect(wrongResponse.status).toBe(404);
    });
  });

  describe("GET /api/linea/{id}/viaje/{id}", () => {
    let linea, punto, parada, viaje, primeraVisita, segundaVisita, response;

    beforeEach(async () => {
      linea = await new Linea({ nombre: "La linea" }).save();
      punto = await new Punto({ latitud: 12, longitud: 21 }).save();
      parada = await new Parada({ linea: linea._id, punto: punto._id }).save();
      viaje = await new Viaje({ linea: linea._id }).save();
    });

    it("should return correct data", async () => {
      response = await request(app).get(
        `/api/linea/${linea.id}/viaje/${viaje.id}`
      );
      const elViaje = await Viaje.findById(viaje._id);
      expect(response.status).toBe(200);
      expect(response.body.viaje._id).toEqual(elViaje.id);
      expect(response.body.viaje.linea.nombre).toEqual(elViaje.linea.nombre);
      expect(response.body.viaje.siguienteParada._id).toEqual(
        elViaje.siguienteParada.id
      );
    });

    it("shold return Visitas", async () => {
      primeraVisita = await new Visita({
        viaje: viaje._id,
        parada: parada._id
      }).save();
      segundaVisita = await new Visita({
        viaje: viaje._id,
        parada: parada._id
      }).save();
      response = await request(app).get(
        `/api/linea/${linea.id}/viaje/${viaje.id}`
      );

      expect(response.body.visitas.length).toBe(2);
    });
  });

  describe("PUT /api/linea/{id}/viaje/{id}", () => {
    let linea,
      viaje,
      punto,
      parada,
      response,
      wrongResponseOnLinea,
      wrongResponseOnViaje,
      wrongResponseOnBoth;

    beforeEach(async () => {
      linea = await new Linea({ nombre: "La linea" }).save();
      punto = await new Punto({ latitud: 12, longitud: 21 }).save();
      parada = await new Parada({ linea: linea._id, punto: punto._id }).save();
      viaje = await new Viaje({ linea: linea._id }).save();
      response = await request(app)
        .put(`/api/linea/${linea._id}/viaje/${viaje._id}`)
        .send({ estaPausado: true, estaCancelado: true });
      wrongResponseOnLinea = await request(app)
        .put(`/api/linea/-123/viaje/${viaje._id}`)
        .send({ estaPausado: true, estaCancelado: true });
      wrongResponseOnViaje = await request(app)
        .put(`/api/linea/${linea._id}/viaje/-123`)
        .send({ estaPausado: true, estaCancelado: true });
      wrongResponseOnBoth = await request(app)
        .put(`/api/linea/-123/viaje/-123`)
        .send({ estaPausado: true, estaCancelado: true });
    });

    it("should return an updated viaje", () => {
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty("estaPausado", true);
      expect(response.body).toHaveProperty("estaCancelado", true);
    });

    it("should return an error if sending wrong data", () => {
      for (const wrongResponse of [
        wrongResponseOnLinea,
        wrongResponseOnViaje,
        wrongResponseOnBoth
      ]) {
        expect(wrongResponse.status).toBe(404);
        expect(wrongResponse.body).toHaveProperty("msg");
      }
    });
  });

  afterEach(async () => {
    await Linea.deleteMany({});
    await Punto.deleteMany({});
    await Parada.deleteMany({});
    await Viaje.deleteMany({});
  });
});
