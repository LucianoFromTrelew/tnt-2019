# tnt-2019

Repositorio para el trabajo final de Taller de Nuevas Tecnologías, 2019. UNPSJB - Trelew

---

- Instalar dependecias del servidor con `npm install` dentro del directorio `server/`
- Levantar contenedores con `docker-compose up`
- Detener y eliminar contenedores con `docker-compose down`
